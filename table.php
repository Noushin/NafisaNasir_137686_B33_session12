<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>

<h2><centre>Book Title-List</centre></h2>


<table>
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td>5</td>
        <td>PHP - BOOK TITLE #1</td>
        <td><button>View</button>
            <button> Edit</button>
            <button> Delete</button>
            <button> Trash</button></td>
    </tr>
    <tr>
        <td>2</td>
        <td>6</td>
        <td>PHP - BOOK TITLE #2</td>
        <td><button>View</button>
            <button> Edit</button>
            <button> Delete</button>
            <button> Trash</button></td>
    </tr>
    <tr>
        <td>3</td>
        <td>7</td>
        <td>PHP - BOOK TITLE #3</td>
        <td><button>View</button>
            <button> Edit</button>
            <button> Delete</button>
            <button> Trash</button></td>
    </tr>

    <tr>
        <td>PAGE : <1,2,3,4,5,6,7></td>
    </tr>

    <tr>
        <td><button>Add New Book Title</button>
            <button>View Trash Items</button>
            <button> Download As PDF</button>
            <button>Download As Excel File</button></td>
    </tr>


</table>

</body>
</html>